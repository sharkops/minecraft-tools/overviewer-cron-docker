FROM debian:stretch

ENV OVERVIEWER_CRON 0 */6 * * *

# Setup S6-overlay
ADD https://github.com/just-containers/s6-overlay/releases/download/v2.2.0.1/s6-overlay-amd64-installer /tmp/
RUN chmod +x /tmp/s6-overlay-amd64-installer && /tmp/s6-overlay-amd64-installer /

# Install Overviewer and Cron
RUN apt-get update && \
    apt-get install -y --no-install-recommends ca-certificates wget gnupg optipng cron vim ncdu htop && \
    echo "deb http://overviewer.org/debian ./" >> /etc/apt/sources.list && \
    wget -O - https://overviewer.org/debian/overviewer.gpg.asc | apt-key add - && \
    apt-get update && \
    apt-get install -y --no-install-recommends minecraft-overviewer && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Setup minecraft user
#RUN groupadd minecraft -g 1000 && useradd -m minecraft -u 1000 -g 1000
#RUN mkdir /home/minecraft/log
#RUN chown minecraft:minecraft -R /home/minecraft/

# Copy init services
COPY ./services.d /etc/services.d

# Configure Overviewer
COPY config/config.py /root/config.py
COPY download_url.py /root/download_url.py
COPY overviewer.sh /root/overviewer.sh
RUN chmod +x /root/overviewer.sh

# Setup entrypoint
COPY entrypoint.sh /root/entrypoint.sh
RUN chmod +x /root/entrypoint.sh

ENTRYPOINT ["bash", "/root/entrypoint.sh"]
