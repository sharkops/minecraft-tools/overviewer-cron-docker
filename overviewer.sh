#!/bin/bash

# The MIT License (MIT)

# Copyright (c) 2016-2020 Mark Ide Jr <https://www.mide.io>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#    The above copyright notice and this permission notice shall be included in all
#    copies or substantial portions of the Software.
#
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#    SOFTWARE.

set -o errexit

# Require VERSION environment variable to be set (no default assumed)
if [ -z "$VERSION" ]; then
  echo "Expecting environment variable VERSION to be set to non-empty string. Exiting."
  exit 1
fi

# Download Minecraft client .jar (Contains textures used by Minecraft Overviewer)
# We only download if the client doesn't exist. In most cases, it won't exist
# because the directory isn't a volume and the Docker container will be a fresh
# slate. This check enables power-users to mount the /home/minecraft/.minecraft/
# directory and prevent downloading if the file exists.
if [ -f "/root/.minecraft/versions/${VERSION}/${VERSION}.jar" ]; then
    echo "Minecraft client ${VERSION}.jar exists! Skipping download."
else
    CLIENT_URL=$(python3 /root/download_url.py "${VERSION}")
    echo "Using Client URL ${CLIENT_URL} to download ${VERSION}.jar."
    mkdir -p "/root/.minecraft/versions/${VERSION}/"
    wget "${CLIENT_URL}" -O "/root/.minecraft/versions/${VERSION}/${VERSION}.jar"
fi

overviewer.py --config "/root/config.py"
