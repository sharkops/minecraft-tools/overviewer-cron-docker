#!/usr/bin/python3

# The MIT License (MIT)

# Copyright (c) 2016-2020 Mark Ide Jr <https://www.mide.io>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
#    The above copyright notice and this permission notice shall be included in all
#    copies or substantial portions of the Software.
#
#    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#    SOFTWARE.


import argparse
import json
import urllib.request

MANIFEST_URL = "https://launchermeta.mojang.com/mc/game/version_manifest.json"


def get_json_from_url(url):
    if not url.startswith("https://"):
        raise RuntimeError("Expected URL to start with https://. It is '{}'.".format(url))
    request = urllib.request.Request(url)
    response = urllib.request.urlopen(request)
    return json.loads(response.read().decode())


def get_minecraft_download_url(version):
    data = get_json_from_url(MANIFEST_URL)

    desired_versions = list(filter(lambda v: v['id'] == version, data['versions']))
    if len(desired_versions) == 0:
        raise RuntimeError("Couldn't find Minecraft Version {} in manifest file {}.".format(version, MANIFEST_URL))
    elif len(desired_versions) > 1:
        raise RuntimeError("Found more than one record published for version {} in manifest file {}.".format(version, MANIFEST_URL))

    version_manifest_url = desired_versions[0]['url']
    data = get_json_from_url(version_manifest_url)

    download_url = data['downloads']['client']['url']
    return download_url


parser = argparse.ArgumentParser()
parser.add_argument("version")
args = parser.parse_args()

# Print out URL for consumption by another program.
print(get_minecraft_download_url(args.version))
